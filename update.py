# -*- coding: utf-8 -*-
"""
Updates a database of stock data files using the latest online stock data from 
Yahoo Finance.

Usage:
    update.py  PATH [--update] [--stocks STOCKS] [--debug]
    update.py  PATH --trim [--stocks STOCKS] [--start START] [--end END]
    update.py  PATH --remove [--stocks STOCKS]
    update.py  -h | --help
    
Options:
    -h --help        Show this screen.
    --update         Updates specified data files located in PATH.
    --trim           Trims specified data files located in PATH to a specified 
                     date range.
    --remove         Deletes stock data files located in PATH.
    --stocks STOCKS  Stocks (files) to be updated or trimmed. If not specified, 
                     all available stocks in PATH will be modified. Enter 
                     multiple stocks as a single space-separated string.                   
    --start START    Earliest date of trimmed data, format YYYY-MM-DD. If not
                     specified, current date will be preserved.
    --end END        Latest date of trimmed data, format YYYY-MM-DD. If not
                     specified, current date will be preserved.
    --debug          Record any failed updates in a log file.
    
"""
from docopt import docopt
from sdmi.database import Database


if __name__ == '__main__':
    args = docopt(__doc__)
    # declare repeatedly used variables
    update = args['--update']
    trim = args['--trim']
    remove = args['--remove']
    stocks = args['--stocks']
    if stocks:
        stocks = stocks.split(' ')
    # create a `Database` instance with the PATH argument
    db = Database(args['PATH'])
    # assume an update if neither --update or --trim are specified
    if update | ((not update) & (not trim) & (not remove)):
        db.update(stocks=stocks, debug=args['--debug'])
    elif trim:
        db.trim(stocks=stocks, start=args['--start'], end=args['--end'])
    else:
        db.remove(stocks=stocks)
