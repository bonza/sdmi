# -*- coding: utf-8 -*-
"""
The `Database` class is used for automated updating for stock data files 
contained within a given 'database' (directory).
    
"""
import os
import pandas as pd
from datetime import datetime
from time import sleep
from sdmi.utils import prepare_stocks
from sdmi.load import Loader, EXTENSION
from sdmi.scrape import YahooScraper

# earliest allowed date in databasef
DB_START = pd.to_datetime('2019-01-01')

# column labels for database files
DB_FILE_COLS = ['date', 'open', 'high', 'low', 'close', 'volume']


class Database(Loader):
    """
    Handles a 'database' containing individual stock data files, which are 
    named according to the stock code, followed by the file extension 
    (e.g. CBA.csv).
    
    Args:
        db_path (str): the location of the database.
    """
    def __init__(self, db_path):
        Loader.__init__(self, db_path)
        self.scraper = YahooScraper()
    
    def _infer_dates(self, stock, start, end):
        """
        Infers start and end dates in preparation for a database update, based 
        on the specified stock, start date, end date, and the contents of the 
        database directory.
        
        Args:
            stock (str): dates will be inferred for this stock.
            start (str or None): start date for the update, format 'YYYY-MM-DD'.
            end (str or None): end datae for the update, format 'YYYY-MM-DD'.
                
        Returns:
            start (Timestamp): start date (possibly inferred) for the update.
            end (Timestamp): end date (possibly inferred) for the update.
            exists (bool): flag indicating whether a data file for `stock` 
                exists within the database.
        """
        # construct hypothetical path to stock's data file
        file = f"{stock}{EXTENSION}"
        # flag indicating whether stock's data file exists, assume true to begin
        exists = True
        # if the file exists
        if file in os.listdir(self.db_path):
            # if 'start' is specified (not None) then use it
            if start:
                start = pd.to_datetime(start)
            else:
                # otherwise try to infer it from existing data
                try:
                    data = self.load(stocks=stock)
                    start = data.index[-1] + pd.Timedelta('1D')
                # if the file is empty, set 'start' to default db start date
                except:
                    start = DB_START
        # if the file doesn't exist switch the flag and set the start date
        else:
            exists = False
            start = pd.to_datetime(start) if start else DB_START
        # if end date is specified (not None), then use it
        if end:
            end = pd.to_datetime(end)
        # otherwise set end date as current date plus a day for good measure
        # (Yahoo can be unpredictable with the dates it returns)
        else:
            end = pd.to_datetime(datetime.now().date()) + pd.Timedelta('1D')
        return start, end, exists
    
    def _update_single_stock(self, stock, start=None, end=None, exchange='asx'):
        """
        Updates data for a single stock (string) in the database. If the stock 
        doesn't exist in the database, then this method will automatically
        create it. See the `update()` method for more info.
        
        """
        filepath = f"{self.db_path}{stock}{EXTENSION}"
        # determine the start and end dates, and whether there is data for the
        # 'stock' in the database
        start, end, exists = self._infer_dates(stock, start=start, end=end)
        # if data file doesn't exist, create it
        if not exists:
            # else create a new file with column names, set start to default
            file = open(filepath, 'w')
            file.write(f"{','.join(DB_FILE_COLS)}\n")
            file.close()
        # scrape the data from Yahoo    
        data = self.scraper.scrape(stock, start, end, exchange=exchange)
        # read in databases's existing data file (ok if empty)
        existing = self.load(stocks=stock)
        # concatenate the two dataframes down the date axis
        updated = pd.concat([existing, data], axis=0)
        # to be extra careful, ensure no duplicate dates exist in the data
        updated.drop_duplicates(inplace=True)
        # sort the data by date
        updated.sort_values(by='date', ascending=True, inplace=True)
        # drop the multiindex column
        updated = updated.droplevel(0, axis=1)
        # add the date index as a column
        updated.reset_index(inplace=True)
        # write data to file, overwriting the existing file
        with open(filepath, 'w+') as f:
            updated.to_csv(f, index=False, header=True)
            
    def update(
        self, stocks=None, start=None, end=None, exchange='asx', debug=False):
        """
        Updates data for `stocks` located within the database, by pulling new
        data from Yahoo Finance.
        
        Args:
            stock (list of str): the stocks to update.
            start (str, default None): the first date of the data update, format 
                'YYYY-MM-DD'. If `None`, then date is inferred from the database 
                as the day after the latest date present there.
            end (str, default None): the last date of the data update, format 
                'YYYY-MM-DD'. If `None`, then today is used.
            exchange (str, default 'asx'): the exchange `stock` belongs to.
            debug (bool): if true, failed updates are printed.
            
        """
        stocks = prepare_stocks(stocks, self.db_path, EXTENSION)
        # try to update each stock, recording unsuccesful updates 
        failures = []
        for stock in stocks:
            try:
                self._update_single_stock(
                    stock, start=start, end=end, exchange=exchange
                )
                # pause briefly so not constantly pinging Yahoo
                sleep(1)
            except:
                failures.append(stock)
        if debug:
            # print out any failed updates
            if failures:
                print('The following stocks failed to update:')
                print(', '.join(failures))
            else:
                print('All stocks updated successfully.')
        
    def trim(self, stocks=None, start=None, end=None):
        """
        Trims data for `stocks` data files to the specified date range. 
        Overwrites the current data file for each stock with it's new trimmed 
        data.
        
        Args:
            stocks (list of str): stocks to be trimmed.
            start (str): the first date of the trimmed data file. If `None`,
                then no left-side trim of the data occurs.
            end (str): the last date of the trimmed data file. If `None`, then
                no right-side trim of the data occurs.
        """
        stocks = prepare_stocks(stocks, self.db_path, EXTENSION)
        for stock in stocks:
            data = self.load(stock)
            # remove the column multiindex, easier if we don't have it here
            data = data.droplevel(0, axis=1)
            # move date index to a new column
            data.reset_index(inplace=True)
            if start:
                start = pd.to_datetime(start, format='%Y-%m-%d')
            else:
                start = data.iloc[0]['date']
            if end:
                end = pd.to_datetime(end, format='%Y-%m-%d')
            else:
                end = data.iloc[-1]['date']
            # trim dataframe by date range
            data = data[(data['date'] >= start) & (data['date'] <= end)]
            # overwrite existing file
            with open(f"{self.db_path}{stock}{EXTENSION}", "w") as f:
                data.to_csv(f, index=False, header=True)
                
    def remove(self, stocks=None):
        """
        Deletes data files for the specified stocks from the database.
        
        Args:
            stocks (str or list): stocks to be deleted. If `None`, deletes the
                database's contents completely.
                
        """
        stocks_orig = stocks
        # get stocks into a list, infer if NoneType provided
        stocks = prepare_stocks(stocks_orig, self.db_path, EXTENSION)
        # text snippet to include in the user input line
        if stocks_orig == None:
            deletes = 'ALL stocks'
        else:
            deletes = ', '.join(stocks)
        # make sure user wants to delete the files!
        reply = input(
            f"Are you sure want to delete {deletes} from the database? (y/n): "
        ).lower()
        # execute the deletion
        if reply == 'y':
            for stock in stocks:
                os.remove(f"{self.db_path}{stock}{EXTENSION}") 
                
