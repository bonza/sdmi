# About

The **S**tock **D**ata **M**anagement **I**nterface, 
or `sdmi` for short, is a `pandas`-based Python API enabling extraction of historical 
stock market data from the web, creation and automated continuous updating of stock 
market data databases, and database retrieval of stock market data. 

The need for this package came about after Yahoo Finance suddenly decommisioned their
historical stock data API in 2017. `sdmi` can serve as a stand-alone package for
anyone interested in maintaining an up-to-date database of stock market data, though
it was primarily developed for my own personal use alongside, and for with interaction with, the `tsbp` backtesting 
package.

# Usage

`sdmi` offers three main modules: `scrape`, `load`, and `database`, culminating
in an interactive bash session script for database management, `update.py`.

## `scrape.py` 

This package deals with extraction of market data from the web (currently only Australian
Stock Exchange data from Yahoo! Finance is available, though this is planned to be expanded 
in the future). Data extraction can be done either via the bash terminal, with the data 
being printed directly as standard output to the terminal, or interactively within a Python 
session using the `scrape.YahooScraper` class. In this case, output is returned as a 
formatted `pandas.DataFrame`. Both of these approaches require the user to specify:

- the stocks they are interested in,
- the start date of the stock data,
- and the end date of the stock data.

It also requires an internet connection!

### Bash

Some example usage of the `scrape` module via the terminal is given below.

For documentation use:

```
python sdmi/scrape.py -h
```

To extract data for the stocks Foo Ltd. (FOO) and Bar Inc. (BAR) for the month of June, use:

```
python sdmi/scrape.py --stocks 'FOO BAR' --start 2019-06-01 --end 2019-06-30
```

The result will be printed to the terminal screen.

### Python

For interacting with the extracted data, extraction should be done within a Python session.
This is achieved using the `YahooScraper` class within the `scrape` module. In this context, the 
above example would look like:

```python
from sdmi.scrape import YahooScraper

ys = YahooScraper()
data = ys.scrape(stocks=['FOO', 'BAR'], start='2019-06-01', end='2019-06-30')
```

The returned `data` variable is a `pandas.DataFrame` object. For more on the 
`YahooScraper` class, see its documentation.

## `load.py`

This module deals with querying stock data from an existing database. It assumes that
the database is a directory containing .csv files for each individual stock, and these
files are named according to the stock's code (e.g. for Foo Ltd. data would contained in
the file /path/to/datase/FOO.csv). As with `scrape`, the `load` module can be used 
either in the terminal or in a Python session. It requires the user to specify:

- the location of the database,

and optionally:

- the stocks they are interested in,
- the start date of the stock data they are querying,
- and the end date of the stock data they are querying.

### Bash

For documentation, use:

```
python sdmi/load.py -h
```

To extract all stock data available within a specified database, use:

```
python sdmi/load.py /path/to/db/
```

To extract data specifically for FOO and BAR for the month of June use:

```
python sdmi/load.py /path/to/db/ --stocks 'FOO BAR' --start 2019-06-01 --end 2019-06-30
```

As with `scrape.py`, the result is printed in the terminal.

### Python

Data retrieval via the `load` module is achieved in a Python session using the `Loader` class.
To extract data for all available stocks use:

```python
from sdmi.load import Loader

loader = Loader('/path/to/database/')
data = loader.load()
```

Alternatively, for FOO and BAR data from the month of June, use:

```
data = loader.load(stocks=['FOO', 'BAR'], start='2019-06-01', end='2019-06-30')
```

## `database.py`

This module handles all creation and management aspects of a stock data database. Use of this
module is restricted to Python sessions, though the accompanying script `update.py`
(described next), enables simple use of the `database` module through a bash session. 
Via the `Database` class, The `database` module offers three main functions on a specified 
database. It assumes that the database adheres to the structure described in the `load.py` 
section. These functions are:

- automated updating of stock data files within the database using historical data from Yahoo! Finance,
- selective removal (trimming) of data within stock data files from the database, and
- deletion of stock data files from the database.

For initialisation, the `Database` class requires the user to specify:

- the location of the database.

To update an existing database to reflect the latest available historical data for
all stocks in the database, and to only update for FOO and BAR, respectively, use:

```python
from sdmi.database import Database

db = Database('/path/to/database/')
db.update()
db.update(stocks=['FOO', 'BAR'])
```

To trim all stock data files so as to only contain data from January to June 2019, use:

```python
db.trim(start='2019-01-01', end='2019-06-30')
```

To trim only FOO and BAR data files, so that their data begins from January 2019, use:

```
db.trim(stocks=['FOO', 'BAR'], start='2019-01-01')
```

To delete all stock data files, and only FOO and BAR data files, respectively, use:

```python
db.remove()
db.remove(stocks=['FOO', 'BAR'])
```

## `update.py`

This script provides a stripped-back bash interface for the `database` module. The
intention is that any basic work using the `database` module should be done via `update.py` 
within a bash session. Usage examples are as follows:

For documenatation, use:

```
python sdmi/update.py -h
```

To update an existing database with the latest available data, use:

```
python sdmi/update.py /path/to/database/ --update
```

To specifically update the stocks FOO an BAR within an existing database, use:

```
python sdmi/update.py /path/to/database/ --update --stocks 'FOO BAR'
```

In addition to `--update`, the `--trim` and `--remove` options are also available. 
For more on this see the documentation.
