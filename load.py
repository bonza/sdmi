# -*- coding: utf-8 -*-
"""
Enables easy loading of stock data into a Python session from a specified 
database.

Usage:
    load.py  PATH [--stocks STOCKS] [--start START] [--end END]
    load.py  -h | --help
    
Options:
    -h --help        Show this screen.
    --stocks STOCKS  Return data for these stocks. Enter multiple stocks as 
                     a single space-separated string.
    --start START    First date of the data (or nearest to), entered in format
                     YYYY-MM-DD.
    --end END        Last date of the data (or nearest to), entered in format 
                     YYYY-MM-DD.
    
"""
import os
import numpy as np
import pandas as pd
from docopt import docopt
from operator import ge, le
from sdmi.utils import prepare_stocks

# file extension of data files within the database
EXTENSION = '.csv'


def make_multiindex_df(data, stock, date_format=None):
    """
    Constructs a new dataframe from `data`, whose columns are a multiindex
    consisting of the stock code at level 0, and the data fields at level 1.
    Reindexes the dataframe using the 'date' column of the original data.
    
    Args:
        data (pandas.DataFrame): dataframe whose columns are to be converted
            to multiindex. Must contain a 'date' column containing date strings.
        stock (str): the stock to which `data` pertains.
        date_format (str, optional): the format of the date strings in the 
            'date' column.
            
    Returns:
        data (pandas.DataFrame): contains the same data as original `data`, but
            with columns now a multiindex consisting of the stock code at level 
            0, and the data fields at level 1. The dataframe has been reindexed 
            using 'date' data from the original `data`, and these dates have been 
            converted from strings to timestamps.
            
    """
    data = data.copy()
    # format existing column names as lowercase
    data.columns = [(stock, column) for column in data.columns]
    # convert the 'date' column entries to datetime objects
    date_column = (stock, 'date')
    data[date_column] = pd.to_datetime(data[date_column], format=date_format)
    # remove any duplicate date entries that unexpectedly exist
    data.drop_duplicates(subset=date_column, inplace=True)
    # make the 'date' column the index of `data`
    data.set_index(date_column, drop=True, inplace=True)
    data.index.names = ['date']
    # convert `data` to a multiindex
    data.columns = pd.MultiIndex.from_tuples(
        list(data.columns), names=['code', 'field']
    )
    # nominal rounding of the data
    data = np.round(data, 3)
    return data


class Loader:
    """
    The Loader` class enables easy loading of stock data from data files
    located within a user-specified database, via the `load` method. Returns 
    data in a pandas.DataFrame which is formatted identically to that retuned 
    by the `scrape.py` module.
    
    Args:
        db_path (str): the absolute location of the database.
        
    Raises:
        OSError: if `db_path` is not an existing path.
        
    """
    def __init__(self, db_path):
        # if last character is not forward slash, put it in
        if db_path[-1] != "/":
            db_path += "/"
        if not os.path.isdir(db_path):
            raise OSError(f"{db_path} does not exist!")
        else:
            self.db_path = db_path
        
    def _load_single_stock(self, stock):
        """
        Loads data for a single specified stock (string) from its corresponding 
        data file within the database. Returns a pandas.DataFrame, where columns 
        are a multiindex consisting of stock code at level 0, and the data fields 
        at level 1. See the `load()` method for more info.
        
        """
        # load in data for the specified stock
        stock = stock.upper()
        db_filepath = f"{self.db_path}{stock}{EXTENSION}"
        data = pd.read_csv(db_filepath)
        # make column names lowercase
        data.columns = [column.lower() for column in data.columns]
        # build multiindex dataframe, cast data as floats
        data = make_multiindex_df(data, stock, "%Y-%m-%d")
        data = data.astype(float)
        return data
        
    def load(self, stocks=None, start=None, end=None):
        """
        Returns data for multiple stocks in a single pandas.DataFrame.
        
        Args:
            stocks (list of str, optional): list of stocks to be loaded. Case 
                insensitive. If not specified, data for all stocks contained 
                within `self.db_path` will be returned.
            start (str, optional): the first date (or nearest to) of the
                returned data, in format 'YYYY-MM-DD'. If not specified, returns
                all available data.
            end (str, optional): the last date (or nearest to) of the returned 
                data, in format 'YYYY-MM-DD'. If not specified, returns all 
                available data.
                
        Returns:
            data (pandas.DataFrame): dataframe containing all available data
                for `stocks` from the database `self.db_path`, spanning `start`
                and `end` dates. Dataframe is indexed by date. Columns are 
                returned as a multiindex, where level 0 corresponds to the
                individual stock codes, and level 1 corresponds to the data 
                fields:
                    - 'open': the opening price on a given date
                    - 'high': the highest traded price on a given date
                    - 'low': the lowest traded price on a given date
                    - 'close': the closing price on a given date
                    - 'volume': the number of units traded on a given date
                    
        Raises:
            FileNotFoundError: if data for any of the specified stocks doesn't 
                exist in the database.
                
        Example:
            >>> loader = Loader('path/to/database/')
            >>> data = loader.load(['CBA', 'BHP'], '2019-01-01', '2019-01-31')
                       
        """
        stocks = prepare_stocks(stocks, self.db_path, EXTENSION)
        data = []
        # create list of individual dataframes, one for each stock
        for stock in stocks:
            stock_data = self._load_single_stock(stock)
            data.append(stock_data)
        # concatenate the list into a single dataframe
        data = pd.concat(data, axis=1)
        # trim data according to the date range if specified
        for clip_to, mode in [(start, ge), (end, le)]:
            if clip_to:
                data = data[mode(data.index, clip_to)]
        return data

 
if __name__ == "__main__":
    args = docopt(__doc__)
    stocks = args['--stocks']
    if stocks:
        args['--stocks'] = stocks.split(' ')
    # pull out the data and print resulting dataframe
    loader = Loader(args['PATH'])
    data = loader.load(
        stocks=args['--stocks'], start=args['--start'], end=args['--end']
    )
    # print the data verbatim
    with pd.option_context(
        "display.max_rows", len(data), "display.max_columns", len(data.columns)
    ):
        print(data)