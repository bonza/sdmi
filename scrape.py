# -*- coding: utf-8 -*-
"""
Enables retrieval and formatting of historical stock data from the Yahoo 
Finance website via a simple API.

Usage:
    scrape.py  --stocks STOCKS --start START --end END [--exchange EXCHANGE]
    scrape.py  -h | --help
    
Options:
    -h --help            Show this screen.
    --stocks STOCKS      Return data for these stocks. Enter multiple stocks as 
                         a single space-separated string.
    --start START        First date of the data (or nearest to), entered in format
                         YYYY-MM-DD.
    --end END            Last date of the data (or nearest to), entered in format 
                         YYYY-MM-DD.
    --exchange EXCHANGE  Exchange to which the stocks belong [default: asx].
    
"""
import requests
import numpy as np
import pandas as pd
from docopt import docopt
from bs4 import BeautifulSoup
from sdmi.load import make_multiindex_df


# url template for yahoo finance historical data for a single stock
YAHOO_URL = (
    "https://finance.yahoo.com/quote/{}{}/history?period1={}&period2={}&" +
    "interval=1d&filter=history&frequency=1d"
)
# only implements Australian Stock Exchange (ASX) currently
EXCHANGE_INFO = {'asx': {'id': '.AX', 'utc_offset': -10}}
# class id details for historical data table html
TABLE_FORMATS = ('table', {'class': 'W(100%) M(0)'})
# columns to keep from the scraped data table, after name formatting
OUTPUT_COLS = ['date', 'open', 'high', 'low', 'close', 'volume']


class URL:
    """
    The `URL` class acts primarily as a url formatter for stock data queries to 
    Yahoo Finance. Specifically, it accepts stock, date, and exchange 
    information, and constructs a url string corresponding to the location of 
    the relevant data online. `URL` is used by the `YahooScraper` class.
    
    Args:
        stock (str): the stock code to be queried (e.g. 'CBA').
        start (str): the desired start date of the data, in format 'YYYY-MM-DD'.
        end (str): the desired end date of the data, in format 'YYYY-MM-DD'.
        exchange (str, optional): the exhange the stock belongs to. Defaults to
            the Australian Stock Exchange ('asx').
    """
    def __init__(self, stock, start, end, exchange='asx'):
        """
        """
        # stored parameters
        self.stock = stock.upper()
        self.start = start
        self.end = end
        # get exchange id used in yahoo finance url
        exchange = exchange.lower()
        exchange_id = EXCHANGE_INFO[exchange]['id']
        # construct buffered start and end times in unix time (used by Yahoo)
        buffer = pd.Timedelta(f'1D')
        start = int((pd.to_datetime(start) - buffer).timestamp())
        end = int((pd.to_datetime(end) + buffer).timestamp())
        # make the url
        self.url = YAHOO_URL.format(self.stock, exchange_id, start, end)


class YahooScraper:
    """
    API for retrieving stock data from the Yahoo Finance website.
    
    """
    @staticmethod
    def _parse_table_from_url(url):
        """
        Pulls historical data for a single stock from the address owned by `url`, 
        and parses it into a rudimentary pandas.DataFrame.
        
        Args:
            url (a URL object): contains the url of the data table to be parsed.
            
        Returns:
            data (pandas.DataFrame): contains the data located at the adress 
                given by `url`, repackaged into a dataframe.
                
        """
        # get html from specified url, pull out the price table
        request = requests.get(url.url)
        soup = BeautifulSoup(request.text, 'html.parser')
        table = soup.find(*TABLE_FORMATS)
        rows = table.find_all('tr')
        # get the table's column names
        columns = []
        for name in rows[0].find_all('th'):
            columns.append(name.get_text())        
        data = []
        # extract the data; need to do this element-wise (row by row, column by 
        # column)
        for row in rows[1:-1]:
            row_data = []
            for column in row.find_all('td'):
                row_data.append(column.get_text())
            data.append(row_data)        
        # put into a DataFrame
        data = pd.DataFrame(data=data, columns=columns) 
        return data
    
    @staticmethod
    def _format_volume_str(volume):
        """
        Yahoo presents volumes with comma formatting (e.g. '1,000'), and '-' in
        instances of no volume. This method reformats these volume strings to 
        float values.
        
        Args:
            volume (str): volume string, potentially containing ',' or '-'.
            
        Returns:
            volume (float): the volume string reformatted to its float value. 
                If original form was '-', this returns 0.0.  
                
        """
        for pair in [(',', ''), ('-', '0')]:
            volume = volume.replace(*pair)
        volume = float(volume)
        return volume
    
    def _format_parsed_table(self, data, url):
        """
        Prettifies the original dataframe produced by `_parse_table_from_url`. 
        
        Args:
            data (pandas.DataFrame): dataframe produced by `_parse_table_from_url`.
            url (a URL object): the url object used by `_parse_table_from_url`.
                
        Returns:
            data (pandas.DataFrame): reformatted dataframe.
            
        """
        # make a copy of `data` to avoid modifying the original
        data = data.copy()
        # get rid of any dividend entries
        data.drop_duplicates('Date', keep='first', inplace=True)
        # format the column names
        data.columns = [
            column.lower().replace('*', '') for column in data.columns
        ]
        # trim data down to desired columns
        data = data[OUTPUT_COLS]
        # convert volume data to desired integer format
        data['volume'] = data['volume'].apply(self._format_volume_str)
        # convert relevant column data to floats ('volume' is already)
        float_cols = ['open', 'high', 'low', 'close']
        data[float_cols] = data[float_cols].replace('-', np.nan)
        data[float_cols] = data[float_cols].astype(float)
        # construct a multiindex dataframe from the data
        data = make_multiindex_df(data, url.stock)
        # trim `data` back to url's original date range
        data = data[(data.index >= url.start) & (data.index <= url.end)]
        return data
    
    def scrape(self, stocks, start, end, exchange='asx'):
        """
        The user-level method to retrieve stock data from Yahoo Finance. Takes
        multiple stocks, along with the desired dates, and corresponding 
        exchange, scrapes the relevant data from the Yahoo Finance website, and 
        returns it in a pandas.DataFrame object.
        
        Args: 
            stocks (list of str): the stock codes to be queried (e.g. 
                ['CBA', 'BHP']).
            start (str): the desired start date of the data, in format 
                'YYYY-MM-DD'.
            end (str): the desired end date of the data, in format 'YYYY-MM-DD'.
            exchange (str, optional): the exhange the stock belongs to. 
                Defaults to the Australian Stock Exchange ('asx').
                
        Returns:
            data (pandas.DataFrame): dataframe containing all available data
                for `stocks` from Yahoo Finance historical data, spanning `start`
                and `end` dates. Dataframe is indexed by date. Columns are 
                returned as a multiindex, where level 0 corresponds to 
                individual stock codes, and level 1 corresponds to the data 
                fields:
                    - 'open': the opening price on a given date
                    - 'high': the highest traded price on a given date
                    - 'low': the lowest traded price on a given date
                    - 'close': the closing price on a given date
                    - 'volume': the number of units traded on a given date
                    
        Example:
            >>> scraper = YahooScraper()
            >>> data = scraper.scrape(['CBA', 'BHP'], '2019-01-01', '2019-01-31')
                    
        """
        # ensure `stocks` is a list, suitable for looping
        if type(stocks) == str:
            stocks = [stocks]
        # go through each stock, pull data from web, put dataframe into list
        data = []
        for stock in stocks:
            # make a url instance
            url = URL(stock, start, end, exchange=exchange)
            # pull the data using the url
            out = self._parse_table_from_url(url)
            # format the data into a nice dataframe
            out = self._format_parsed_table(out, url)
            data.append(out)
        # concatenate the list into a single dataframe
        data = pd.concat(data, axis=1)        
        return data
    

if __name__ == "__main__":
    args = docopt(__doc__)
    args['--stocks'] = args['--stocks'].split(' ')
    # pull out the data and print resulting dataframe
    scraper = YahooScraper()
    data = scraper.scrape(
        args['--stocks'], args['--start'], args['--end'], args['--exchange']
    )
    # print the data verbatim
    with pd.option_context(
        "display.max_rows", len(data), "display.max_columns", len(data.columns)
    ):
        print(data)
                  