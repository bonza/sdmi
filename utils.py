# -*- coding: utf-8 -*-
"""
Some basic functions used repeatedly in the sdmi package's other modules.

"""
import os


def _infer_stocks(db_path, ext):
    """
    Returns a list of stock codes based on existing data files within a
    specified database.
    
    Args:
        path (str): path to database.
        ext (str): file extension of stock data files within database.
        
    Returns:
        stocks (list of str): list of stock codes corresponding to existing
            data files in database.
            
    """
    stocks = [
        stock.replace(ext, '') for stock in os.listdir(db_path) 
        if ext in stock
    ]
    return stocks


def prepare_stocks(stocks, db_path, extension):
    """
    If `stocks` is NoneType, function infers the stock list from all stock data 
    files contained in `db_path`. If `stocks` is a single stock (str), the
    function puts it in a list.
    
    Args:
        stocks (str, list, or NoneType): stock(s) to be list-ified, and
            possibly inferred.
        db_path (str): the location of the database.
        extension (str): the file extension of the stock data files contained
            in `db_path`.
    
    Returns:
        stocks (list of str).
        
    """
    # if stocks aren't specified, get all from database
    if not stocks:
        stocks = _infer_stocks(db_path, extension)
    # ensure `stocks` is a list
    if type(stocks) == str:
        stocks = [stocks]
    return stocks
